# Libre Assistant

This is a fork of the official [Home Assistant companion app](https://github.com/home-assistant/android).
It removes all non-free code and dependencies from the app making it Free Software.

No additional functionality is added
and at present, the missing functionality is not replaced with free alternatives.

Removed non-free functionality:
  * Push messages with Firebase (FCM/GCM)
  * Location Tracking with Play Services (could be replaced)
  * Translations with Lokalise SDK

## Workflow

The current patch set is maintained in the `libre` branch.
The `master` branch is automatically pulled from upstream and should not be touched.
Releases are meant to track upstream as closely as possible
with as little modifications as possible.

A release is identified by a tag that uses the same version number as upstream,
but adds a `libre-` prefix.
The patch set in a given release can be found by checking the tag history.

When a new upstream version is released,
the `libre` branch will be rebased on top of the upstream tag,
eventual conflicts will be resolved
and the new path set will be force pushed into the `libre` branch
and a new release tagged.

The number of commits rebased on upstream releases should also be as few as possible,
but logically separated.

Currently, there are:

* a commit to remove non-free dependencies (the only one where we should get conflicts)
* a commit to rebrand the upstream app (and add this README) to avoid trademark issues
* a release commit adding a new version code and version name (unique per tag)
