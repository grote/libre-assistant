plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
    id("com.github.triplet.play") version "2.7.5"
}

buildscript {
    repositories {
        google()
        maven(url = Config.Repository.gradle)
    }
    dependencies {
        classpath(Config.Plugin.androidJunit5)
    }
}

android {
    compileSdkVersion(Config.Android.compileSdk)

    ndkVersion = Config.Android.ndk

    defaultConfig {
        applicationId = "io.homeassistant.companion.android"
        minSdkVersion(Config.Android.minSdk)
        targetSdkVersion(Config.Android.targetSdk)

        val ver = System.getenv("VERSION") ?: "LOCAL"
        val vCode = System.getenv("VERSION_CODE")?.toIntOrNull() ?: 1
        versionCode = vCode
        versionName = "$ver-$vCode"
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    signingConfigs {
        create("release") {
            storeFile = file("release_keystore.keystore")
            storePassword = System.getenv("KEYSTORE_PASSWORD") ?: ""
            keyAlias = System.getenv("KEYSTORE_ALIAS") ?: ""
            keyPassword = System.getenv("KEYSTORE_ALIAS_PASSWORD") ?: ""
            isV1SigningEnabled = true
            isV2SigningEnabled = true
        }
    }

    buildTypes {
        named("debug").configure {
            applicationIdSuffix = ".debug"
        }
        named("release").configure {
            isDebuggable = false
            isJniDebuggable = false
            isZipAlignEnabled = true
            signingConfig = signingConfigs.getByName("release")
        }
    }

    testOptions {
        unitTests.apply { isReturnDefaultValues = true }
    }

    tasks.withType<Test> {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }

    lintOptions {
        disable("MissingTranslation")
    }
}

play {
    serviceAccountCredentials = file("playStorePublishServiceCredentialsFile.json")
    track = "beta"
    resolutionStrategy = "ignore"
}

dependencies {
    implementation(project(":common"))
    implementation(project(":domain"))

    implementation(Config.Dependency.Misc.blurView)

    implementation(Config.Dependency.Kotlin.core)
    implementation(Config.Dependency.Kotlin.coroutines)
    implementation(Config.Dependency.Kotlin.coroutinesAndroid)

    implementation(Config.Dependency.Google.dagger)
    kapt(Config.Dependency.Google.daggerCompiler)

    implementation(Config.Dependency.AndroidX.appcompat)
    implementation(Config.Dependency.AndroidX.lifecycle)
    implementation(Config.Dependency.AndroidX.constraintlayout)
    implementation(Config.Dependency.AndroidX.recyclerview)
    implementation(Config.Dependency.AndroidX.preference)
    implementation(Config.Dependency.Google.material)

    implementation(Config.Dependency.AndroidX.roomRuntime)
    implementation(Config.Dependency.AndroidX.roomKtx)
    kapt(Config.Dependency.AndroidX.roomCompiler)

    implementation(Config.Dependency.Misc.threeTenAbp) {
        exclude(group = "org.threeten")
    }

    implementation(Config.Dependency.Misc.jackson)

    implementation(Config.Dependency.AndroidX.workManager)
    implementation(Config.Dependency.AndroidX.biometric)

    implementation("com.squareup.okhttp3:okhttp:3.12.0")
    implementation("com.google.code.gson:gson:2.8.6")

    testImplementation(Config.Dependency.Testing.spek2Jvm)
    testImplementation(Config.Dependency.Testing.spek2JUnit)
    testImplementation(Config.Dependency.Testing.assertJ)
    testImplementation(Config.Dependency.Testing.mockk)
    testImplementation(Config.Dependency.Kotlin.coroutinesTest)
}


android {
    flavorDimensions("liberty")
    productFlavors {
        create("libre") {
            applicationId = "de.grobox.libreassistant"
            setDimension("liberty")
            versionCode = 215
            versionName = "1.11.1-$versionCode"
        }
    }
}
